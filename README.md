# README #

### What is this repository for? ###

**pyhdf** provides a Dockerfile to build the `paterno/pyhdf` Docker image, and a `makefile` to bring some order to tagging, testing, and pushing to Docker Hub.

### How do I get set up? ###

1. Clone this repository.
2. Run `make` (no target needed). This will build current version of the image.
3. Modify the `Dockerfile` as needed, adding tests to `test/runner.sh`
4. When a new stable state is reached, bump the version number in the `makefile`. Run `make tag`.
5. When you're ready to push to Docker Hub, run `make release`. Follow the instructions to commit your new `Dockerfile`, tests, etc.

### Contribution guidelines ###

Please fork the repository, and send pull requests.
