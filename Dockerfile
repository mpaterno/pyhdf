FROM fnalart/hdf5_larsoftobj
RUN yum install -y numactl-devel
WORKDIR /products
RUN curl http://scisoft.fnal.gov/scisoft/packages/mpich/v3_2_0/mpich-3.2.0-slf6-x86_64-e10-prof.tar.bz2 | tar xj
RUN echo 'setup -B mpich v3_2_0 -q +e10:+prof' > /etc/mpich_setup.sh
RUN echo 'source /etc/mpich_setup.sh' >> /etc/profile
WORKDIR /tmp
RUN curl https://pypi.python.org/packages/ee/b8/f443e1de0b6495479fc73c5863b7b5272a4ece5122e3589db6cd3bb57eeb/mpi4py-2.0.0.tar.gz#md5=4f7d8126d7367c239fd67615680990e3 | tar xz
RUN  source /etc/profile && \
     cd /tmp/mpi4py-2.0.0 && \
     python setup.py install --prefix /products/mpi4py/v2_0_0/Linux64bit+2.6-2.12-e10-prof && \
     echo 'export PYTHONPATH=/products/mpi4py/v2_0_0/Linux64bit+2.6-2.12-e10-prof/lib/python2.7/site-packages:$PYTHONPATH' > /etc/mpi4py_setup.sh && \
     echo 'export PATH=/products/mpi4py/v2_0_0/Linux64bit+2.6-2.12-e10-prof/bin:$PATH' >> /etc/mpi4py_setup.sh && \
     echo 'source /etc/mpi4py_setup.sh' >> /etc/profile
#RUN  yum update ; yum -y install java
#WORKDIR /tmp
#RUN curl https://download.jetbrains.com/python/pycharm-community-2016.1.4.tar.gz | tar xz
#RUN mv /tmp/pycharm-community-2016.1.4  /opt/pycharm-community
#RUN ln -s /opt/pycharm-community/bin/pycharm.sh /usr/local/bin/pycharm
#RUN ln -s /opt/pycharm-community/bin/inspect.sh /usr/local/bin/inspect
#RUN rm -rf /tmp/* ; yum clean all
