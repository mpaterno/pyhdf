NAME = paterno/pyhdf
VERSION = 0.0.3

.PHONY: all build tag_latest release test shell verify_host

all: build

verify_host:
	@if [ -f /.dockerenv ]; then echo 'Run make from host, not container' ; false; fi

build: verify_host
	docker build --tag $(NAME):$(VERSION) $(PWD)

test: verify_host
	@env NAME=$(NAME) VERSION=$(VERSION) ./test/runner.sh

tag_latest: verify_host
	docker tag $(NAME):$(VERSION) $(NAME):latest

release: test tag_latest
	@if ! docker images $(NAME) | awk '{ print $$2 }' | grep -q -F $(VERSION); then echo "$(NAME) version $(VERSION) is not yet built. Please run 'make build'"; false; fi
	docker push $(NAME)
	@echo "*** Don't forget to create a tag. git tag rel-$(VERSION) && git push origin rel-$(VERSION)"

shell: build
	docker run --name "try-pyhdf" --rm -it --volume $(PWD):/work --workdir /work $(NAME):$(VERSION)
