#!/usr/bin/env python
from __future__ import print_function
from mpi4py import MPI

comm = MPI.COMM_WORLD
if comm is None:
    print("Failed to access MPI.COMM_WORLD")


