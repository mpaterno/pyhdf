die()
{
  local exitval
  if [[ "$1" =~ ^[0-9]*$ ]]; then (( exitval = $1 )); shift; else (( exitval = 1 )); fi
  echo "ERROR: $@" 1>&2
  exit $exitval
}

STD_ARGS="--rm --volume $PWD/test:/test --workdir /test"
IMAGE="${NAME}:${VERSION}"

docker run ${STD_ARGS} ${IMAGE} bash -cl "type -p mpiexec > /dev/null" || die "mpiexec not found"
echo "mpiexec found"

docker run --rm  --volume $PWD/test:/test --workdir /test ${NAME}:${VERSION} bash -cl "mpiexec -n 1 python test_mpi4py.py > /dev/null" ||  die "loading mpi4py failed"
echo "loading mpi4py worked"
